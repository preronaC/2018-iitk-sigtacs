\documentclass[dvipsnames]{beamer}
\usetheme{focus}

\usepackage[english] {babel}
\input{macros}
\usepackage{amsmath,amssymb,amsfonts,graphicx}
\usepackage{commons}
\usepackage{tikz}
\usetikzlibrary{shapes,arrows}
\usetikzlibrary{matrix}
\usetikzlibrary{calc}

\newcommand{\tmark}[1]{\tikz[remember picture, overlay] \node(#1){};}

\newcommand{\zero}{\mathrm{0}}
\renewcommand{\phi}{\varphi}

\title{Faithful Homomorphisms and PIT \\ \hfill -- a short survey}
\author
  {Prerona Chatterjee\\ \vspace{.5em} \small{based on joint work with \emph{Ramprasad Saptharishi}}}

\date{November 3, 2018}
\institute{IIT Kanpur}

\begin{document}
\tikzset{%
	block/.style    = {draw, thick, rectangle, minimum height = 3em,
		minimum width = 3em},
	sum/.style      = {draw, circle, node distance = 2cm}, % Adder
	input/.style    = {coordinate}, % Input
	output/.style   = {coordinate} % Output
}
\newcommand{\suma}{\Large$+$}
\newcommand{\proda}{\Large$\times$}

\maketitle

\section{Introduction}

\begin{frame}{Polynomial Identity Testing}
	\begin{tikzpicture}[thick, node distance=2cm]
		\draw
		node [name=dummy] {}
		node [sum, right of=dummy] (top) {\suma}
		node [sum] at ($(top)+(-2,-1.5)$) (mult1) {\proda}
		node [sum, right of=mult1] (mult2) {\proda}
		node [sum, right of=mult2] (mult3) {\proda}
		node [sum] at ($(top)+(-3,-3)$) (sum1) {\suma}
		node [sum, right of=sum1] (sum2) {\suma}
		node [sum, right of=sum2] (sum3) {\suma}
		node [sum, right of=sum3] (sum4) {\suma}
		node [sum] at ($(top)+(-2,-4.5)$) (var1) {$x_1$}
		node [sum, right of=var1] (var2) {$x_2$}
		node [sum, right of=var2] (var3) {$x_3$};
		
		\draw[->](top) -- ($(top)+(0cm,1cm)$);
		\draw[->](mult1) -- node[left] {$\alpha_1$} (top);
		\draw[->](mult2) -- node[right] {$\alpha_2$} (top);
		\draw[->](mult3) -- node[right] {$\alpha_3$} (top); 
		\draw[->](sum1) -- (mult1);
		\draw[->](sum1) -- (mult2);
		\draw[->](sum2) -- (mult3);
		\draw[->](sum3) -- (mult2);
		\draw[->](sum4) -- (mult1);
		\draw[->](sum4) -- (mult3);
		\draw[->](var1) -- (sum1);
		\draw[->](var1) -- (sum2);
		\draw[->](var1) -- (sum3);
		\draw[->](var2) -- (sum1);
		\draw[->](var2) -- (sum2);
		\draw[->](var2) -- (sum4);
		\draw[->](var3) -- (sum3);
		\draw[->](var3) -- (sum4);   
		
		\node at (1.5,.8) {$\ckt$};
		\node at ($(top) + (5.5,0)$) {$\ckt \stackrel{?}{=} \zero$};
    	\draw ($(top) + (4.5,-0.5)$) rectangle ($(top) + (6.5,0.5)$);
	\end{tikzpicture}
\end{frame}

\begin{frame}{The Trivial Solution \& What is Required}
	\textbf{Given}: $n$-variate, degree $d$ polynomial\\
	\textbf{Size of trivial hitting set}: \textcolor{BrickRed}{$(d+1)^n$} \pause
	
	\vspace{1.5em}
	\textbf{Approach}: Reduce number of variables + Preserve non-zeroness \pause
	
	\vspace{1em}
	\textbf{Trivial Substitution}: $x_i \to t^{(d+1)^i}$ \\
	distinct monomials $\to$ distinct power in $t$ $\implies$ \textcolor{ForestGreen}{No Cancellations}\\ \pause
	\textbf{No. of variables}: \textcolor{ForestGreen}{1} \qquad  \textbf{Degree}: \textcolor{BrickRed}{$(d+1)^n$} \qquad
	\textbf{|Hitting Set|}: \textcolor{BrickRed}{$(d+1)^n$} \pause
	
	\vspace{2em}
	\begin{tabbing}
		\textbf{What we need}: \= Reduce no. of variables (preferably constant)\\
		\> Keep degree under control (poly-bounded)\\
		\> Preserve non-zeroness
	\end{tabbing}
\end{frame}

\begin{frame}{A Special Setting: Can we do better?}
	\begin{columns}
		\begin{column}{.3\textwidth}
			\begin{tikzpicture}[thick]
			\uncover<1->{\draw (0,0) circle (.3);
			\draw[->] (0,.3) -- (0,1);
			\node at (.5,.8) {$\ckt$};}
			\uncover<1>{
				\draw (-.2,-.25) -- (-1.6,-3.2) -- (1.6,-3.2) -- (.2,-.25);
				\draw (-.3,-1.5) circle (.3);
				\node at (-.3,-1.5) {$\times$};
				\draw (.5,-2.2) circle (.3);
				\node at (.5,-2.2) {$+$};
				\node at (-1.3,-3.7) {$x_1$};
				\node at (-.8,-3.7) {$x_2$};
				\node at (-.1,-3.6) {$\cdots$};
				\node at (.5,-3.6) {$\cdots$};
				\node at (1.3,-3.7) {$x_n$};
				\draw (-1.3,-3.4) -- (-1.3,-3);
				\draw (-.8,-3.4) -- (-.8, -3);
				\draw (1.3,-3.4) -- (1.3,-3);}
			\uncover<2->{
				\node at (0,-1.2) {$\ckt'$};
				\draw (-.2,-.25) -- (-1.5,-2) -- (1.5,-2) -- (.2,-.25);
				\draw (-1.1,-1.9) -- (-1.1,-2.2) -- (-1.6,-2.8) -- (-.6,-2.8) -- (-1.1,-2.2);
				\node at (-1.1,-2.55) {$f_1$};
				\node at (-.2,-2.55) {$\cdots$};
				\node at (.2,-2.55) {$\cdots$};
				\node at (1.1,-2.55) {$f_m$};
				\draw (1.1,-1.9) -- (1.1,-2.2) -- (1.6,-2.8) -- (.6,-2.8) -- (1.1,-2.2);
				\draw (-1.4,-2.7) -- (-1.4,-3);
				\draw (-1.3,-2.7) -- (-1.3,-3);
				\node at (-1.05,-3) {$\cdots$};
				\draw (-.8,-2.7) -- (-.8,-3);
				\draw (1.4,-2.7) -- (1.4,-3);
				\draw (1.3,-2.7) -- (1.3,-3);
				\node at (1.05,-3) {$\cdots$};
				\draw (.8,-2.7) -- (.8,-3);
				\draw (-1.3,-3.6) circle (.2);
				\draw (-.5,-3.6) circle (.2);
				\node at (.1,-3.6) {$\cdots$};
				\node at (.6,-3.6) {$\cdots$};
				\draw (1.3,-3.6) circle (.2);
				\draw (-1.3,-3.4) -- (-1.3,-3.2);
				\draw (-.5,-3.4) -- (-.5, -3.2);
				\draw (1.3,-3.4) -- (1.3,-3.2);
				\node at (-1.3,-3.6) {\tiny{$x_1$}};
				\node at (-.5,-3.6) {\tiny{$x_2$}};
				\node at (1.3,-3.6) {\tiny{$x_n$}};}
			\end{tikzpicture}
		\end{column}
	\begin{column}{.575\textwidth}
		\uncover<1->{Check whether $\ckt$ computes the zero polynomial or 	 	
			not.\\ \vspace{1em}}
		\uncover<2->{$\ckt = \ckt'(\fm)$\\ \vspace{1em}}
		\uncover<3->{Only $k$ of them are "relevant" where 
			\[k < < n\]\\ \vspace{1em}}
		\uncover<4->{
			\begin{center}
				\textbf{Can we do any better than trivial?}
			\end{center}}
	\end{column}
	\end{columns}
\end{frame}

\section{Preliminaries}

\begin{frame}{Algebraic Independence}
\textbf{Definition}: Suppose $\set{f_1, \ldots, f_k} \subseteq \F[\x]$. They are said to be algebraically dependent if there exists $A \in \F[y_1, \ldots, y_k]$ such that 
\[A(y_1, \ldots, y_k) \neq \zero; \qquad A(f_1, \ldots, f_k) = \zero.\]
Otherwise, they are said to be algebraically independent.

\vspace{1em}
\begin{columns}
	\begin{column}{.35\textwidth}
		\begin{tikzpicture}[thick]
		\draw (0,0) circle (.3);
		\draw[->] (0,.3) -- (0,1);
		\node at (.6,.8) {$\ckt \neq \zero$};
		\draw (-.2,-.25) -- (-1.5,-2) -- (1.5,-2) -- (.2,-.25);
		
		\node at (-1.1,-2.55) {$y_1$};
		\node at (-.2,-2.55) {$\cdots$};
		\node at (.2,-2.55) {$\cdots$};
		\node at (1.1,-2.55) {$y_k$};
		
		\draw (-1.1,-1.8) -- (-1.1,-2.2);
		\draw (1.1,-1.8) -- (1.1,-2.2);
		\end{tikzpicture}
	\end{column}
	\begin{column}{.35\textwidth}
		\begin{tikzpicture}[thick]
		\draw (0,0) circle (.3);
		\draw[->] (0,.3) -- (0,1);
		\node at (.6,.8) {$\ckt = \zero$};
		\draw (-.2,-.25) -- (-1.5,-2) -- (1.5,-2) -- (.2,-.25);
		\node at (-1.1,-2.55) {$f_1$};
		\node at (-.2,-2.55) {$\cdots$};
		\node at (.2,-2.55) {$\cdots$};
		\node at (1.1,-2.55) {$f_k$};
		\draw (-1.1,-1.8) -- (-1.1,-2.2);
		\draw (1.1,-1.8) -- (1.1,-2.2);
		\end{tikzpicture} \pause
	\end{column}
	\begin{column}{.25\textwidth}
		\begin{block}{Algebraic Rank of $\set{f_1, \ldots, f_m}$}
			Size of the maximal algebraically independent subset of $\set{f_1, \ldots, f_m}$. 
		\end{block}
	\end{column}
\end{columns}
\end{frame}

\begin{frame}{Our Setting: How is it different?}
	\[\text{$\ckt = \ckt'(f_1, \ldots f_m)$ : $\algrank(f_1, \ldots, f_m) = k << n$}\]
	\begin{columns}
		\begin{column}{.3\textwidth}			
			\begin{tikzpicture}[thick]
			\draw (0,0) circle (.3);
			\draw[->] (0,.3) -- (0,1);
			\node at (.5,.8) {$\ckt$};
			\node at (0,-1.2) {$\ckt'$};
			\draw (-.2,-.25) -- (-1.5,-2) -- (1.5,-2) -- (.2,-.25);
			\draw (-1.1,-1.9) -- (-1.1,-2.2) -- (-1.6,-2.8) -- (-.6,-2.8) -- (-1.1,-2.2);
			\node at (-1.1,-2.55) {$f_1$};
			\node at (-.2,-2.55) {$\cdots$};
			\node at (.2,-2.55) {$\cdots$};
			\node at (1.1,-2.55) {$f_m$};
			\draw (1.1,-1.9) -- (1.1,-2.2) -- (1.6,-2.8) -- (.6,-2.8) -- (1.1,-2.2);
			\draw (-1.4,-2.7) -- (-1.4,-3);
			\draw (-1.3,-2.7) -- (-1.3,-3);
			\node at (-1.05,-3) {$\cdots$};
			\draw (-.8,-2.7) -- (-.8,-3);
			\draw (1.4,-2.7) -- (1.4,-3);
			\draw (1.3,-2.7) -- (1.3,-3);
			\node at (1.05,-3) {$\cdots$};
			\draw (.8,-2.7) -- (.8,-3);
			\draw (-1.3,-3.6) circle (.2);
			\draw (-.5,-3.6) circle (.2);
			\node at (.1,-3.6) {$\cdots$};
			\node at (.6,-3.6) {$\cdots$};
			\draw (1.3,-3.6) circle (.2);
			\draw (-1.3,-3.4) -- (-1.3,-3.2);
			\draw (-.5,-3.4) -- (-.5, -3.2);
			\draw (1.3,-3.4) -- (1.3,-3.2);
			\node at (-1.3,-3.6) {\tiny{$x_1$}};
			\node at (-.5,-3.6) {\tiny{$x_2$}};
			\node at (1.3,-3.6) {\tiny{$x_n$}};
			\end{tikzpicture} \pause
		\end{column}
		\begin{column}{.6\textwidth}
			\textbf{Easy Case}: $k = m$\\
			Have access to $\ckt'$ \pause
			
			\vspace{1em}
			\textbf{|Hitting Set| in this case}: ${(d+1)^k}$ \pause
			
			\vspace{2em}
			\textbf{General Case}: $k \neq m$\\
			Do not have access to $\ckt'$ \pause
			
			\vspace{1em}
			\textbf{What is allowed}: Substitute $x_i$s			
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}{Our Setting: What can be done?}
	\begin{columns}
		\begin{column}{.3\textwidth}			
			\begin{tikzpicture}[thick]
				\draw (0,0) circle (.3);
				\draw[->] (0,.3) -- (0,1);
				\node at (.5,.8) {$\ckt_\zero$};
				\node at (0,-1.2) {$\ckt'$};
				\draw (1.3,-3.6) circle (.2);
				\draw (-.2,-.25) -- (-1.5,-2) -- (1.5,-2) -- (.2,-.25);
				\draw (-1.1,-1.9) -- (-1.1,-2.2) -- (-1.6,-2.8) -- (-.6,-2.8) -- (-1.1,-2.2);
				\node at (-1.1,-2.55) {$f_1$};
				\node at (-.2,-2.55) {$\cdots$};
				\node at (.2,-2.55) {$\cdots$};
				\node at (1.1,-2.55) {$f_m$};
				\draw (1.1,-1.9) -- (1.1,-2.2) -- (1.6,-2.8) -- (.6,-2.8) -- (1.1,-2.2);
				\draw (-1.4,-2.7) -- (-1.4,-3);
				\draw (-1.3,-2.7) -- (-1.3,-3);
				\node at (-1.05,-3) {$\cdots$};
				\draw (-.8,-2.7) -- (-.8,-3);
				\draw (1.4,-2.7) -- (1.4,-3);
				\draw (1.3,-2.7) -- (1.3,-3);
				\node at (1.05,-3) {$\cdots$};
				\draw (.8,-2.7) -- (.8,-3);
				\draw (-1.3,-3.6) circle (.2);
				\draw (-.5,-3.6) circle (.2);
				\node at (.1,-3.6) {$\cdots$};
				\node at (.6,-3.6) {$\cdots$};
				\draw (1.3,-3.6) circle (.2);
				\draw (-1.3,-3.4) -- (-1.3,-3.2);
				\draw (-.5,-3.4) -- (-.5, -3.2);
				\draw (1.3,-3.4) -- (1.3,-3.2);
				\node at (-1.3,-3.6) {\tiny{$\phi_1$}};
				\node at (-.5,-3.6) {\tiny{$\phi_2$}};
				\node at (1.3,-3.6) {\tiny{$\phi_n$}};
				\draw (-1.4,-3.75) -- (-1.6,-4.1) -- (-1,-4.1) -- (-1.2,-3.75);
				\draw (-.6,-3.75) -- (-.8,-4.1) -- (-.2,-4.1) -- (-.4,-3.75);
				\draw (1.2,-3.75) -- (1,-4.1) -- (1.6,-4.1) -- (1.4,-3.75);
				\draw (-1.5,-4.05) -- (-1.5,-4.2);
				\draw (-1.4,-4.05) -- (-1.4,-4.2);
				\draw (-1.1,-4.05) -- (-1.1,-4.2);
				\draw (-.7,-4.05) -- (-.7,-4.2);
				\draw (-.6,-4.05) -- (-.6,-4.2);
				\draw (-.3,-4.05) -- (-.3,-4.2);
				\draw (1.1,-4.05) -- (1.1,-4.2);
				\draw (1.2,-4.05) -- (1.2,-4.2);
				\draw (1.5,-4.05) -- (1.5,-4.2);
				\node at (-1.3,-5) {$y_1$};
				\node at (-.7,-5) {$y_2$};
				\node at (-.05,-5) {$\cdots$};
				\node at (.5,-5) {$\cdots$};
				\node at (1.3,-5) {$y_k$};
				\draw (-1.3,-4.4) -- (-1.3,-4.7);
				\draw (-.7,-4.4) -- (-.7, -4.7);
				\draw (1.3,-4.4) -- (1.3,-4.7);
			\end{tikzpicture}
		\end{column}
		\begin{column}{.65\textwidth}
			$x_i \to \phi_i(y_1, \ldots, y_k)$
			
			\vspace{1em}
			\textbf{Property required}: $\ckt \neq \zero \implies \ckt_\zero \neq \zero$ \pause
			
			\vspace{2em}
			For $k = m$, \textbf{sufficient property} of $\phi_i'$s:
			\[g_i = f_i(\phi_1, \ldots, \phi_n)\]
			$\set{f_1, \ldots, f_k}$ are A.I. $\Leftrightarrow$ $\set{g_1, \ldots, g_k}$ are A.I. \pause
			
			\vspace{2em}
			\textbf{Amazing fact} [BMS'11, ASSS'12]:\\
			Works even in the general case $(k < m)$.
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}{Faithful Maps}
\textbf{Definition}: Let $\set{f_1, \ldots, f_m} \subseteq \F[\vecx]$ with $\algrank(f_1, \ldots, f_m)= k$. A map 

\vspace{-2em} \[\phi: \set{x_1, \ldots x_n} \to \F[\yk]\]
is said to be faithful if $\algrank(g_1, \ldots, g_m) = k$.\\ 
Here $g_i(\vecy) = f_i(\phi(x_1), \ldots, \phi(x_n))$.\pause

\vspace{1.5em}
\textbf{Why should one believe that such maps can exist?}\\
\underline{What we know}: If $\set{f_1, \ldots, f_k}$ are algebraically independent, then 

\vspace{-1em}
\[\F(f_1, \ldots, f_k) \cong \F(y_1, \ldots, y_k).\]\pause
\underline{What we are asking for}: A better way of seeing this isomorphism. 
That is, are there $g_1, \ldots, g_k \in \F[y_1, \ldots y_k]$ such that $g_i = f_i(\phi(x_1), \ldots, \phi(x_n))$ and

\vspace{-1em}
\[\F(f_1, \ldots, f_k) \cong \F(g_1, \ldots, g_k)?\]
\end{frame}

\begin{frame}{Constructing Faithful Maps are enough}
	\begin{block}{\textbf{Lemma} [BMS'11, ASSS'12]}
		If $\phi$ is a faithful map for $\set{f_1, \ldots, f_m} \subseteq \F[\vecx]$, then for any $\ckt$
		\[\ckt(f_1, \ldots, f_m) \neq \zero \implies \ckt(f_1 \circ \phi, \ldots, f_m \circ \phi) \neq \zero\] 
	\end{block} \pause

	\begin{columns}
		\begin{column}{.65\textwidth}
			\textbf{Proof}: Let $g_i = f_i(\phi(x_1), \ldots, \phi(x_n))$.
			
			\vspace{.5em}
			$\ckt[f_1, \ldots, f_m] \neq \zero \Rightarrow \ckt(f_1, \ldots, f_m) \neq \zero$\\
			$\Rightarrow \ckt(f_1, \ldots, f_m) \cdot R(f_1, \ldots, f_m) = 1$\\
			$\Rightarrow \ckt(f_1, \ldots, f_m) \cdot R'[f_1, \ldots, f_m] = Q[f_1, \ldots, f_k]$\\
			$\Rightarrow \ckt(g_1, \ldots , g_m) \cdot R'[g_1, \ldots , g_m]$\\ 
			\qquad \qquad \qquad \qquad \qquad $= Q[g_1, \ldots , g_k] \neq \zero$\\
			$\Rightarrow \ckt(g_1, \ldots, g_m) = \ckt(f_1 \circ \phi, \ldots, f_m \circ \phi) \neq \zero$
		\end{column}
	\begin{column}{.35\textwidth}
		\begin{block}{Useful fact}
			$\F(f_1, \ldots, f_m)$\\
			\qquad $|$ \qquad \qquad is an
			$\F(f_1, \ldots, f_k)$\\
			algebraic extension.\\
			Thus, $R(f_1, \ldots, f_m)$
			
			\vspace{-1.5em}
			\[= \frac{R'[f_1, \ldots, f_m]}{Q[f_1, \ldots, f_k]}\]
		\end{block}
	\end{column}
	\end{columns}
\end{frame}

\begin{frame}{Linear Rank Extractors [GR'05]}
	\[\begin{bmatrix}
	\text{ } & \text{ } & \text{ } & \text{ } & \text{ } & \text{ } & \text{ }\\
	\text{ }& \text{ } & \text{ } & \text{ } & \text{ } & \text{ } & \text{ }\\ 
	& \text{ } & \text{ } & \text{ } & A & \text{ } & \text{ } & \text{ } &\\
	\text{ } & \text{ } & \text{ } & \text{ } & \text{ } & \text{ } & \text{ }\\
	\text{ } & \text{ } & \text{ } & \text{ } & \text{ } & \text{ } & \text{ }
	\end{bmatrix}
	\times 
	\begin{bmatrix}
	s & \ldots & s^k\\
	\inparen{s^2}^1& \ldots & \inparen{s^2}^k\\
	\vdots & \text{ } & \vdots\\
	\vdots & \ddots & \vdots\\
	\vdots & \ddots & \vdots\\
	\vdots & \text{ } & \vdots\\
	\inparen{s^n}^1 & \ldots & \inparen{s^n}^k
	\end{bmatrix}
	=
	\begin{bmatrix}
	& \text{ } & \text{ } & \text{ } & \text{ } & \text{ } &\\
	& \text{ } & \text{ } & \text{ } & \text{ } & \text{ } &\\ 
	& \text{ } & \text{ } & A' & \text{ } & \text{ } &\\
	& \text{ } & \text{ } & \text{ } & \text{ } & \text{ } &\\
	& \text{ } & \text{ } & \text{ } & \text{ } & \text{ } &
	\end{bmatrix}\]
	
	\begin{align*}
		&A \text{ has full } \F \text{-rank }\\
		&\implies \det(A') \text{ is a non-zero polynomial in } s \text{ of degree at most } nk^2\\
		&\implies A' \text{ has full } \F(s) \text{-rank}\\
		&\implies \text{For some } s \in \set{\zero, \ldots, nk^2} A' \text{ has full } \F \text{-rank}
	\end{align*}
\end{frame}

\begin{frame}{Capturing Algebraic Rank via Linear Rank}
For $\fm \in \F[\x]$ and $\vecf = (\fm)$, 
\[\J_\vecx(\vecf) = 
\begin{bmatrix} %change order
\partial_{x_1}(f_1) & \partial_{x_2}(f_1) & \ldots & \partial_{x_n}(f_1)\\
\partial_{x_1}(f_2) & \partial_{x_2}(f_2) & \ldots & \partial_{x_n}(f_2)\\
\vdots & \vdots & \ddots & \vdots\\
\partial_{x_1}(f_m) & \partial_{x_2}(f_m) & \ldots & \partial_{x_n}(f_m)\\
\end{bmatrix}\] \pause
\begin{block}{The Jacobian Criterion}
	If $\F$ has characteristic zero, the algebraic rank of $\set{\fm}$ is equal to the linear rank of its Jacobian matrix.
\end{block} 
\end{frame}


\section{Characteristic Zero Fields:\newline [Jac'41], [BMS'11], [ASSS'12]}

\begin{frame}{The Goal}
\textbf{What we need}: \qquad $\J_\vecx(\vecf)$ has full rank $\implies$ $\J_\vecy(\vecf \circ \phi)$ has full rank \pause

\vspace{1.5em}
\textbf{What we can do}: Write $\J_\vecy(\vecf \circ \phi)$ in terms of $\J_\vecx(\vecf)$. \pause

\vspace{.5em}
Suppose $\phi$ is a generic linear map: \qquad
$\phi : x_i = \sum_{j=1}^{k}a_{ij}y_j + b_i.$ \pause

Then for $M_\phi[i,j] = a_{ij}$,
\[\begin{bmatrix}
\text{ } & \text{ } & \text{ }\\
\text{ } & \text{ } & \text{ }\\
\text{ } & \J_\vecy(\vecf \circ \phi) & \text{ }\\ 
\text{ } & \text{ } & \text{ }\\
\text{ } & \text{ } & \text{ }\\
\end{bmatrix}
=
\begin{bmatrix}
& \text{ } & \text{ } & \text{ } & \text{ } & \text{ } &\\ 
& \text{ } & \text{ } & \text{ } & \text{ } & \text{ } &\\
& \text{ } & \text{ } & \phi(\J_\vecx(\vecf)) & \text{ } & \text{ } &\\
& \text{ } & \text{ } & \text{ } & \text{ } & \text{ } &\\
& \text{ } & \text{ } & \text{ } & \text{ } & \text{ } &
\end{bmatrix}
\times 
\begin{bmatrix}
& \text{ } & \text{ } & \text{ } &\\
& \text{ } & \text{ } & \text{ } &\\
& \text{ } & \text{ } & \text{ } &\\
& \text{ } & M_\phi & \text{ } &\\ 
& \text{ } & \text{ } & \text{ } &\\
& \text{ } & \text{ } & \text{ } &\\
& \text{ } & \text{ } & \text{ } &\\
\end{bmatrix}\]
\end{frame}

\begin{frame}{The Revised Goal \& The Solution}
\begin{tabbing}
	\textbf{What we need}: \qquad \= $M_\phi[i,j] = s^{ij}$: Easy (define $a_{ij}$ to be $s^{ij}$)\\ \pause
	\> $\J_\vecx(\vecf)$ has full rank $\implies$ $\phi(\J_\vecx(\vecf))$ has full rank \pause
\end{tabbing}

\vspace{1em}
\textbf{How to ensure $\J_\vecx(\vecf)$ has full rank $\implies$ $\phi(\J_\vecx(\vecf))$ has full rank}:
\begin{itemize}
	\item $\mathcal{S}$: Hitting set for the family of all $k \times k$ minors of $\J_\vecx(\vecf)$ \pause
	\item Choose $\vecb$ to be points from $\mathcal{S}$ \pause
\end{itemize}

\vspace{2em}
\textbf{What we get}: \qquad $\mathcal{F} = \set{\phi_{s, \vecb} = \sum_{j=1}^{k} s^{ij}y_j + b_i}_{s \in \set{\zero, \ldots, nk^2}, \vecb \in \mathcal{S}}$ \pause

\vspace{.5em}
\textbf{Guarantee}: \qquad If $\ckt = \ckt'(f_1, \ldots, f_m) \neq \zero$, then for some $\phi \in \mathcal{F}$, 

\vspace{-1em}
\[\ckt_\zero = \ckt'(f_1 \circ \phi, \ldots, f_m \circ \phi) \neq \zero.\]
\end{frame}

\begin{frame}{Some Instantiations}
	\begin{block}{The PIT Algorithm}
		\textbf{Input}: \qquad $\ckt = \ckt'(f_1, \ldots, f_m)$, $k \geq \algrank(f_1, \ldots, f_m)$ \pause
		
		\vspace{.5em}
		\textbf{Step 1}: For every $\phi \in \mathcal{F}$ not tried yet.\\
		\qquad \qquad Run trivial PIT on $\ckt_\zero = \ckt'(f_1 \circ \phi, \ldots, f_m \circ \phi)$\\ 
		\qquad \qquad If $\ckt_\zero \neq \zero$, return  "NOT ZERO".\\
		\textbf{Step 2}: Return "ZERO". \pause
	\end{block}
	
	\vspace{1em}
	\textbf{When is it useful?} \qquad $k$ is a constant and $\abs{\mathcal{S}} \leq \poly(n)$.\\ \pause
	
	\vspace{1em}
	\textbf{Some Examples}: \qquad $\ckt = \ckt'(f_1, \ldots, f_m)$ where\\
	\begin{itemize}
		\item each $f_i$ is a sparse polynomial
		\item each $f_i$ is a product of multilinear, variable disjoint, sparse polynomials
		\item each $f_i$ is a product of linear polynomials
	\end{itemize}
\end{frame}

\section{Arbitrary Fields:\\ The PSS Criterion [PSS'16]}

\begin{frame}{What goes wrong over arbitrary fields?}
Jacobian Matrix has partial derivatives as entries \pause
- Entries can start becoming zero \pause 
: \qquad Not the only case.\\ \pause
\vspace{1em}
\begin{center}
	$f_1 = xy^{p-1}$, $f_2 = x^{p-1}y$ : Algebraically Independent over $\F_p$.\pause
\end{center}
\[\J_{x,y} = \begin{bmatrix}
y^{p-1} & (p-1)xy^{p-2}\\
(p-1)x^{p-2}y & x^{p-1}
\end{bmatrix}\]
\[\det(\J_{x,y}) = (xy)^{p-1} - (p^2 - 2p + 1) (xy)^{p-1} = \zero \text{ over }\F_p.\] \pause

\textbf{Characteristic Zero}: \qquad $\J$ has full rank $\Leftarrow$ $\J$ has an inverse\\ \pause
\vspace{1em}
\textbf{Finite Characteristic}: Entries in "inverse" have denominators that are partial derivatives of some annihilators - Can become zero.
\end{frame}

\begin{frame}{Looking Further in the Taylor Expansion}
For any $f \in \F[\x]$ and $\vecz \in \F^n$,
\[f(\vecx + \vecz) - f(\vecz) = \underbrace{x_1 \cdot \partial_{x_1}f + \cdots + x_n \cdot \partial_{x_n}f}_{\text{Jacobian}} + \text{ higher order terms}\]\pause

[PSS'16]: Look at Taylor expansions up to the "inseparable degree". \pause

\vspace{1em}
\begin{columns}
\begin{column}{.55\textwidth}
	\begin{block}{A New Operator}
		For any $f \in \F[\x]$,
		\[\Ech_t(f) = \deg^{\leq t} \inparen{f(\vecx + \vecz) - f(\vecz)}\]
	\end{block}
\end{column}\pause
\begin{column}{.45\textwidth}
	\[\hat{\Ech}(\vecf) = \begin{bmatrix}
	& \ldots & \Ech_t(f_1) & \ldots & \\
	& \ldots & \Ech_t(f_2) & \ldots & \\
	& & \vdots\\
	& \ldots & \Ech_t(f_k) & \ldots &
	\end{bmatrix}.\]
\end{column}
\end{columns}
\end{frame}

\begin{frame}{The PSS Criterion}
A given set of polynomials $\set{\fk} \in \F[\x]$ is algebraically independent if and only if for a random $\vecz \in \F^n$, $\set{\Ech_t(f_1), \Ech_t(f_2), \ldots, \Ech_t(f_k)}$ are linearly independent in  \[\frac{\F(\vecz)[\x]}{\I_t}\] where $t$ is the inseparable degree of $\set{\fk}$ and 
\[
\I_t = \genset{\Ech_t(f_1), \Ech_t(f_2), \ldots, \Ech_t(f_k)}^{\geq 2}_{\F(\vecz)} \bmod{\genset{\vecx}^{t+1}} \subseteq \F(\vecz)[\vecx].
\]
\end{frame}

\begin{frame}{Alternate Statement for the PSS Criterion}

$\set{\fk}$ is algebraically independent if and only if for every $\inparen{v_1, v_2, \ldots, v_k}$ with $v_i \text{s in } \I_t$,
\[\Ech(\vecf, \vecv) = \begin{bmatrix}
& \ldots & \Ech_t(f_1) + v_1 & \ldots & \\
& \ldots & \Ech_t(f_2) + v_2 & \ldots & \\
& & \vdots\\
& \ldots & \Ech_t(f_k) + v_k & \ldots &
\end{bmatrix} \text{ has full rank over } \F(\vecz).\]
\end{frame}

\begin{frame}{The Goal}	
	\textbf{Define}: $\mathcal{F} = \set{\phi : \set{x_1, \ldots, x_n} \to \F[y_1, \ldots y_k]}$ with $\abs{\mathcal{F}} \approx \poly(n)$.
	
	\vspace{.5em}
	\textbf{Required}: \qquad $\ckt = \ckt'(f_1, \ldots, f_m) \neq \zero$, $\algrank(f_1, \ldots, f_m) \leq k$\\
	\qquad \qquad $\implies$ for some $\phi \in \mathcal{F}$, $\ckt_\zero = \ckt'(f_1 \circ \phi, \ldots, f_m \circ \phi) \neq \zero.$ \pause
	
	\vspace{1.5em}	
	\textbf{Sufficient}: \qquad Let $g_i = f_i \circ \phi$ and $\Ech_t(g) = \deg^{\leq t} \inparen{g(\vecy + \vecv) - g(\vecv)}$. Then,
	\[\Ech(\vecf \circ \phi, \vecu) = \begin{bmatrix}
	& \ldots & \Ech_t(f_1 \circ \phi) + u_1 & \ldots & \\
	& \ldots & \Ech_t(f_2 \circ \phi) + u_2 & \ldots & \\
	& & \vdots\\
	& \ldots & \Ech_t(f_m \circ \phi) + u_m & \ldots &
	\end{bmatrix}\] 
	\begin{center}
		has full rank for every $u_1, u_2, \ldots, u_m \in \I_t(\phi)$ where
		\[
		\I_t(\phi) = \genset{\Ech_t(g_1), \ldots, \Ech_t(g_m)}^{\geq 2}_{\F(\vecg(\vecv))} \bmod{\genset{\vecy}^{t+1}} \subseteq \F(\vecv)[\vecy].
		\]
	\end{center}
\end{frame}

\section{Constructing Faithful Homomorphisms over Arbitrary Fields}

\begin{frame}{What to ask for from the map}
\[\phi : x_i \to \sum_{j=1}^{k}a_{ij}y_j + b_iy_\zero \qquad \text{ and } \qquad \phi_z : z_i \to \sum_{j=1}^{k}a_{ij}w_j + b_iw_\zero\] \pause

\vspace{1em}
\underline{\textbf{Sufficient Properties}}
\begin{enumerate}
\vspace{.5em}
\item For every $\vecu \in \I_t(\phi)$, there is a $\vecv \in \I_t$ for which $\vecu = \phi_z(\vecv \circ \phi)$\\ \pause
\vspace{.5em}
\item $\Ech(\vecf \circ \phi, \vecu) = \phi_z(\Ech(\vecf,\vecv)) \times M_\phi$: Chain Rule \pause
\vspace{.5em}
\item $\rank(\Ech(\vecf, \vecv)) = \rank(\phi_z(\Ech(\vecf, \vecv)))$: $b_i$s are responsible for this \pause
\vspace{.5em}
\item $M_\phi$ preserves rank
\end{enumerate}
\end{frame}

\begin{frame}{The Matrix Decomposition}	
\[\hspace{-1em}
\begin{bmatrix}
\text{ } & \text{ } & \text{ } \\
\text{ } & \Ech(\vecf \circ \phi, \vecu) & \text{ } \\
\text{ } & \text{ } & \text{ } 
\end{bmatrix}
=
\overbrace{\begin{bmatrix}
& \text{ } & \text{ } & \text{ } & \text{ } & \text{ } &\\ 
& \text{ } & \text{ } & \phi_z(\Ech(\vecf,\vecv)) & \text{ } & \text{ } &\\
& \text{ } & \text{ } & \text{ } & \text{ } & \text{ } &
\end{bmatrix}}^{\text{ labelled by }\vecx^\vece}
\times 
\underbrace{\begin{bmatrix}
& & \text{ } & \text{ } & \text{ } & &\\
& & \text{ } & \text{ } & \text{ } & &\\
& & \text{ } & \text{ } & \text{ } & &\\
& & \text{ } & M_{\phi} & \text{ } & &\\ 
& & \text{ } & \text{ } & \text{ } & &\\
& & \text{ } & \text{ } & \text{ } & &\\
& & \text{ } & \text{ } & \text{ } &
\end{bmatrix}}_{\text{labelled by } \vecy^\vecd}
\]\pause
where
\[
M_\phi(\vecx^\vece, \vecy^\vecd) = \left\{
\begin{array}{ll}
\coeff_{\vecy^\vecd}(\phi(\vecx^\vece)) & \mbox{if } \sum e_i = \sum d_i \\
\zero & \mbox{ otherwise} 
\end{array}
\right.
\]
\end{frame}

\begin{frame}{What makes Vandermonde type matrices work?}
\only<1-2>{\[\begin{bmatrix}
	& \text{ } & \text{ } & \text{ } & \text{ } & \text{ } &\\ 
	& \text{ } & \text{ } & A & \text{ } & \text{ } &\\
	& \text{ } & \text{ } & \text{ } & \text{ } & \text{ } &
	\end{bmatrix}
	\times 
	\begin{bmatrix}
	\text{ } & \text{ } & \text{ }\\
	\text{ } & \text{ } & \text{ }\\
	\text{ } & \text{ } & \text{ }\\
	\text{ } & M & \text{ }\\ 
	\text{ } & \text{ } & \text{ }\\
	\text{ } & \text{ } & \text{ }\\
	\text{ } & \text{ } & \text{ }
	\end{bmatrix}
	=
	\begin{bmatrix}
	& \text{ } &\\
	& AM &\\ 
	& \text{ } &
	\end{bmatrix}\]
	
	\vspace{2em}}

\only<2->{ 
	\begin{center}
		\textbf{Cauchy-Binet}: $\det(AM) = \sum_{B \subseteq \set{x_i} \text{, } \abs{B} = k} \det(A_B) \det(M_B).$
\end{center}}

\only<3>{\[\begin{bmatrix}
	s & s^2 & \ldots & s^k\\
	s^2 & s^4 & \ldots & s^{2k}\\
	\vdots & \vdots & \text{ } & \vdots\\
	\vdots & \vdots & \ddots & \vdots\\
	\vdots & \vdots & \ddots & \vdots\\
	\vdots & \vdots & \text{ } & \vdots\\
	s^n & s^{2n} & \ldots & s^{kn}
	\end{bmatrix}\]}
\only<4>{\[\begin{bmatrix}
	s & \ldots & s^k\\
	\inparen{s^2}^1& \ldots & \inparen{s^2}^k\\
	\vdots & \text{ } & \vdots\\
	\vdots & \ddots & \vdots\\
	\vdots & \ddots & \vdots\\
	\vdots & \text{ } & \vdots\\
	\inparen{s^n}^1 & \ldots & \inparen{s^n}^k
	\end{bmatrix}\]}
\only<5->{\[\begin{array}{r}
	x_1\\
	x_2\\
	\vdots\\
	\vdots\\
	\vdots\\
	\vdots\\
	x_n
	\end{array}
	\begin{bmatrix}
	\inparen{s^{\wt(x_1)}}^1 & \ldots & (s^{\wt(x_1)})^k\\
	\inparen{s^{\wt(x_2)}}^1& \ldots & \inparen{s^{\wt(x_2)}}^k\\
	\vdots & \text{ } & \vdots\\
	\vdots & \ddots & \vdots\\
	\vdots & \ddots & \vdots\\
	\vdots & \text{ } & \vdots\\
	\inparen{s^{\wt(x_n)}}^1 & \ldots & \inparen{s^{\wt(x_n)}}^k
	\end{bmatrix} 
	\quad \quad 
	\only<5-8>{\wt(x_i) = i}
	\only<9->{\wt(x_i) \text{ is distinct for each } i}\]}

\only<6->{\begin{itemize}
		\item If $B = \inparen{x_{i_1}, x_{i_2}, \ldots, x_{i_k}}$, then $\wt(B) = \sum_{j=1}^{k} j \wt(x_{i_j})$ \pause
		\uncover<7->{\item $\deg_s(\det(M_B)) = \wt(B)$} \pause
		\uncover<8->{\item Isolate a unique non-zero minor of $A$ with maximum weight}
\end{itemize}}	
\end{frame}

\begin{frame}{The Current Matrix}
	\begin{tikzpicture}
	\draw[thick] (0cm,0cm) rectangle (4cm,6cm); 
	\node(rows) at (-.5cm, 3cm) {$\vecx^\vece$};
	\draw[->, thick] ($(rows)+(0cm,.5cm)$) -- (-.5cm,6cm);
	\draw[->, thick] ($(rows)+(0cm,-.5cm)$) -- (-.5cm,0cm);
	\node(cols) at (2cm, -.5cm) {$\vecy^\vecd$};
	\draw[->, thick] ($(cols)+(.5cm,0cm)$) -- (4cm,-.5cm);
	\draw[->, thick] ($(cols)+(-.5cm,0cm)$) -- (0cm,-.5cm);
	\draw[thick] (.5cm,5.25cm) rectangle (0cm,6cm);
	\node(one) at (.25cm, 5.625cm) {$1$};
	\draw[thick] (.5cm,5.25cm) rectangle (1.25cm,4.25cm);
	\node(two) at (.825cm, 4.75cm) {$2$};
	\node(ddots1) at (1.5cm, 3.75cm) {$\ddots$};
	\node(ddots2) at (2cm, 3cm) {$\ddots$};
	\draw[thick] (2.5cm,2.5cm) rectangle (4cm,0cm);
	\node(tee) at (3.25cm, 1.25cm) {$t$};	\node(entry) at (7.5cm, 5.5cm) {$M_\phi(\vecx^\vece, \vecy^\vecd) = \coeff_{\vecy^\vecd}(\phi(\vecx^\vece))$};\pause
	\node at (7.5cm, 4cm) {\textbf{Taking inspiration from the}};
	\node at (7.5cm, 3.5cm) {\textbf{prev. case}: $M_\phi(x_i, y_j) = s^{\wt(i)j}$}; \pause
	\node at (7.75cm, 2.75cm) {$\wt(\vecx^\vece) = \sum_{i \in [n]} e_i \wt(i)$};
	\pause
	\node at (7.75cm, 2cm) {$M_\phi(\vecx^\vece, y_j^d) = s^{\wt(\vecx^\vece)j}$};
	\pause
	\node at (7.75cm, .75cm) {If $B = \inparen{\vecx^{\vece_1}, \vecx^{\vece_2}, \ldots, \vecx^{\vece_k}}$,};
	\node at (7.75cm, 0cm) {then $\wt(B) = \sum_{j \in [k]} j \wt(\vecx^{\vece_j})$};
	\end{tikzpicture}
\end{frame}

\begin{frame}{A Rank Preserving Matrix}
\[\uncover<2->{\begin{array}{r}
\uparrow\\
k\\
\downarrow\\
\end{array}}
\begin{bmatrix}
& \text{ } & \tmark{a}\text{ } & \text{ } & \text{ } & \text{ } &\\ 
& \text{ } & \text{ } & A & \text{ } & \text{ } &\\
& \text{ } & \text{ } & \text{ } & \text{ }\tmark{b} & \text{ } &
\end{bmatrix}
\times 
\only<1-4>{\begin{bmatrix}
& \text{ } & \text{ } & \text{ } &\\
& \text{ } & \text{ } & \text{ } &\\
& \text{ } & \text{ } & \text{ } &\\
& \text{ } & M & \text{ } &\\ 
& \text{ } & \text{ } & \text{ } &\\
& \text{ } & \text{ } & \text{ } &\\
& \text{ } & \text{ } & \text{ } &
\end{bmatrix}
=
\begin{bmatrix}
\text{ } & \text{ } & \text{ }\\
\text{ } & AM & \text{ }\\ 
\text{ } & \text{ } & \text{ }
\end{bmatrix}}
\only<5->{\begin{bmatrix}
\text{ } & \text{ } & \text{ }\\
\text{ } & \text{ } & \text{ }\\
\text{ } & \text{ } & \text{ }\\
\text{ } & M' & \text{ }\\ 
\text{ } & \text{ } & \text{ }\\
\text{ } & \text{ } & \text{ }\\
\text{ } & \text{ } & \text{ }
\end{bmatrix}
=
\begin{bmatrix}
& \text{ } &\\
& AM' &\\ 
& \text{ } &
\end{bmatrix}}
\]\pause
\begin{tikzpicture}[remember picture, overlay]
\only<2-4>{\node (t) at ($(6.325cm, -.125cm)$) {$>k$};
\draw[->, thick] ($(5.825cm, -.125cm)$) -- ($(5.125cm, -.125cm)$);
\draw[->, thick] ($(6.825cm, -.125cm)$) -- ($(7.5cm, -.125cm)$);}
\only<5->{\node (t) at ($(6.325cm, -.125cm)$) {$k$};
\draw[->, thick] ($(6.125cm, -.125cm)$) -- ($(5.5cm, -.125cm)$);
\draw[->, thick] ($(6.5cm, -.125cm)$) -- ($(7.325cm, -.125cm)$);}
\only<4->{\draw[blue, thick] ($(a)+(.05cm,.1cm)$) rectangle ($(b)-(.05cm,0cm)$);} 
\end{tikzpicture}\pause

\vspace{2em}
\textbf{What we want}: $k$ columns of $AM$ that are linearly independent.\\ \pause
\vspace{1em}
\textbf{Proof Strategy}: 
\begin{itemize}
\item Isolate a unique non-zero minor $A_{B_\zero}$ with maximum weight \pause
\item $M' \equiv$ $k$ columns of $M$ such that $\deg_s(\det(M'_{B_\zero})) = \wt(B_\zero)$
\end{itemize}
\end{frame}

\begin{frame}{A few details}
\textbf{About $\deg_s(\det(M'_{B_\zero}))$ for $B \neq B_\zero$}:\\
\begin{itemize}
\item $\deg_s(\det(M'_B)) \leq \wt(B)$ for $B \neq B_\zero$ \pause
\end{itemize}
\vspace{1em}
\textbf{About $\wt$}:\\
\begin{itemize}
\item $\wt$ "hashes" the monomials in question\\ $\Rightarrow$ there is a unique $B$ of maximum weight. \pause
\end{itemize}
\vspace{1em}
\textbf{About $M'$}
\begin{itemize}
\item $M'$ can always be chosen such that its columns are indexed by "pure" monomials.
\end{itemize}
\end{frame}

\begin{frame}{A Faithful Map over Arbitrary Fields}
\vspace{-1.5em}
\[\phi: x_i \to \sum_{j=1}^{k}s^{\wt(i)j} y_j + a_iy_\zero \quad \text{ and } \quad \phi_z: z_i \to \sum_{j=1}^{k}s^{\wt(i)j} w_j + a_iy_\zero\] where $t$ is the inseparable degree and $\wt(i) = (t+1)^i \bmod p$.\pause \\
\vspace{1em}
\underline{\textbf{Properties}}
\begin{enumerate}
\vspace{.5em}
\item For every $\vecu \in \I_t(\phi)$, there is a $\vecv \in \I_t$ for which $\vecu = \phi_z(\vecv \circ \phi)$\\ \pause
\vspace{.5em}
\item $\phi_z(\Ech(\vecf,\vecv)) \times M_\phi$ is a sub-matrix of $\Ech(\vecf \circ \phi, \vecu)$\\ \pause
\vspace{.5em}
\item $\rank(\phi_z(\Ech(\vecf,\vecv))) = \rank(\phi_z(\Ech(\vecf,\vecv)) \times M_\phi)$\\ \pause
\vspace{.5em}
\item $\rank(\Ech(\vecf, \vecv)) = \rank(\phi_z(\Ech(\vecf,\vecv)))$
\end{enumerate}
\end{frame}

\begin{frame}{Open Threads}
\begin{enumerate}
\item Improve the dependence on "inseparable degree".\\ \pause
\vspace{2em}
\item $\text{[GSS'18]}$: Different characterisation for Algebraic dependence - not algorithmic but has no dependence on "inseparable degree"\\
\vspace{1em}
Can we get PIT applications out of it?\pause
\end{enumerate}
\vspace{3em}
\begin{center}
\textbf{\large{Thank you!}}
\end{center}
\end{frame}
\end{document}